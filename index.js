
const subscribe = ( state ) => ( fn ) => {
    var thisId = state.id ++;
    state.subs[ thisId ] = fn;
    return () => delete state.subs[ thisId ];
};

const trigger = ( state ) => ( event = null ) => {
    for( var id of Object.keys( state.subs ) ) {
        if( state.subs[ id ] )
            state.subs[ id ]( event );
    }
};

const subject = () => {

    var state = ({ subs : {}, id : 0 });

    return {
        subscribe : subscribe( state ),
        trigger   : trigger( state )
    };
};

module.exports = { subject };
